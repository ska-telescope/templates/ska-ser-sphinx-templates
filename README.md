# ska-ser-sphinx-templates

:bulb: SKA templates for `sphinx` documentation

By using this, your project will also be up-to-date with the latest SKAO look.

This repo should be added as a `git submodule` to `sphinx` projects

## Usage
Add the submodule to your project:

```bash
cd my-ska-project-using-sphinx
mkdir -p docs/src
cd docs/src
git submodule add https://gitlab.com/ska-telescope/templates/ska-ser-sphinx-templates.git
git add .gitmodules ska-ser-sphinx-templates
git commit -s
```

After this bootstrap the configuration:

```bash
./ska-ser-sphinx-templates/bootstrap.sh
```

After modifiying the required files commit the bootstrapped files to your repo.

Now we can add the the actual documentation!


## Upgrading existing repositories

* create a branch for the change, then

Run the above procedure, i.e.

```
cd docs/src
git submodule add https://gitlab.com/ska-telescope/templates/ska-ser-sphinx-templates.git
git add .gitmodules ska-ser-sphinx-templates
git commit -s
./ska-ser-sphinx-templates/bootstrap.sh
```

Then compare the changes and restore/modify as needed before committing, specifically _../requirements.txt_ and _conf.py_.
