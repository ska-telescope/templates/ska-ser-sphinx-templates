#!/bin/bash

MYDIR=$(dirname "$0")
REPO=$(basename $(git rev-parse --show-toplevel))
REPO_UNDER=$(echo $REPO | sed -e 's/-/_/g')
if [ ! -f ../requirements.txt ]; then
    echo "Adding 'requirements.txt' to 'docs' directory"
    cp $MYDIR/requirements.txt ../
else
    echo "Overriding 'requirements.txt' in parent directory. Please cherry pick the changes before committing."
    cp $MYDIR/requirements.txt ../
    git diff ../requirements.txt
fi
if [ ! -f index.rst ]; then
    echo "Adding basic 'index.rst'"
    sed -e "s/%REPO_UNDER%/$REPO_UNDER/" $MYDIR/index.rst > index.rst
fi
if [ ! -f conf.py ]; then
    echo "Adding 'conf.py'"
    sed -e "s/%REPOSITORY%/$REPO/" $MYDIR/conf.py > conf.py
    echo "Please edit these variables to reflect your project's settings!"
    grep 'REPLACE_ME' conf.py
else
    echo "Changing 'conf.py'. Please cherry pick the changes before committing"
    sed -e "s/%REPOSITORY%/$REPO/" $MYDIR/conf.py > conf.py
    git diff conf.py
fi
if [ -d _static ]; then
    echo "removing old '_static' directory on upgrade."
    rm -rf _static
fi